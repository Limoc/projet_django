from django.apps import AppConfig


class MicrollyAppConfig(AppConfig):
    name = 'microlly_app'
