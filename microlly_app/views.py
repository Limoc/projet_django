from microlly_app.models import Publication
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.shortcuts import render, redirect , get_object_or_404
from microlly_app import forms
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required
# Create your views here.

# def index(request):
#     publis = Publication.objects.all()
#     return render(request, '_base.html', {'latests_publis': publis})

def publi(request, slug):
    publi = get_object_or_404(Publication, slug=slug)
    return render(request, 'publi/details.html', {'publi': publi})

def v_login(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('microlly_app:index')
    else:
        form = AuthenticationForm()
    return render(request, 'users_pages/login.html', {'form': form})


def v_logout(request):
    if request.method == 'POST':
        logout(request)
        return redirect('microlly_app:index')
    else:
        return redirect('microlly_app:index')


def v_signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('microlly_app:index')
    else:
        form = UserCreationForm()
    return render(request, 'users_pages/signup.html', {'form': form})

def index(request):
    publis_list = Publication.objects.all().order_by('-creation_date')
    paginator = Paginator(publis_list, 9)
    page = request.GET.get('page')
    publis = paginator.get_page(page)
    return render(request, '_base.html', {'latest_publis': publis})


def p_details(request, slug, pk):
    publi = get_object_or_404(Publication, slug=slug, pk=pk)
    return render(request, 'publi/details.html', {'publi': publi})


@login_required(login_url="users_pages/login/")
def p_create(request):
    if request.method == 'POST':
        form = forms.PubliCreate(request.POST)
        if form.is_valid():
            publi = form.save(commit=False)
            publi.author = request.user
            publi.save()
            return redirect('microlly_app:index')
    else:
        form = forms.PubliCreate()
    return render(request, 'publi/p_create.html', {'form': form})


@login_required(login_url="users_pages/login/")
def p_edit(request, slug, pk):
    publi = get_object_or_404(Publication, slug=slug, pk=pk)
    form = forms.PubliEdit(request.POST or None, instance=publi)
    if request.user != publi.author:
        return redirect('microlly_app:index')
    if request.method == 'POST':
        if form.is_valid():
            publi.save()
            return redirect('microlly_app:index')
    context = {
        "form": form,
        "publi": publi,
    }
    return render(request, 'publi/p_edit.html', context)


@login_required(login_url="users_pages/login/")
def p_user_list(request, author):
    author = get_object_or_404(User, username=author)
    publis_list = Publication.objects.filter(author=author).order_by('-creation_date')
    paginator = Paginator(publis_list, 9)
    page = request.GET.get('page')
    publis = paginator.get_page(page)

    context = {
        "author": author,
        "p_user_list": publis,
    }

    return render(request, 'publi/p_user_list.html', context)

@login_required(login_url="users_pages/login/")
def p_delete(request, slug, pk):
    publi = get_object_or_404(Publication, pk=pk)
    if request.user != publi.author:
        return redirect('microlly_app:index')
    if request.method == 'POST':
        publi.delete()
        return redirect('microlly_app:index')
