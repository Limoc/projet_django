from django.urls import include, path
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from microlly_app import views
from django.views.generic.base import TemplateView # new
app_name = 'microlly_app'


urlpatterns = [
    # path('accounts/', include('django.contrib.auth.urls')),
    path('', views.index, name='index'),
    path('login/', views.v_login, name='login'),
    path('logout/', views.v_logout, name='logout'),
    path('signup/', views.v_signup, name='signup'),
    path('create/', views.p_create, name='create'),
    path('bloger/<str:author>', views.p_user_list, name='p_user_list'),
    path('publi/<slug:slug>/<int:pk>', views.p_details, name='details'),
    path('details/<slug:slug>/<int:pk>', views.p_details, name='details'),
    path('edit/<slug:slug>/<int:pk>', views.p_edit, name='edit'),
    path('delete/<slug:slug>/<int:pk>', views.p_delete, name='delete'),

]