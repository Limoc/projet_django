from django.test import TestCase
from django.urls import reverse
from django.db.models.query import QuerySet
from microlly_app.models import Publication


class WebsiteTestCase(TestCase):
    fixtures = ['initial.json', ]

    def test_index_page(self):
        response = self.client.get(reverse('microlly_app:index'))
        self.assertContains(response, "Latests publi")
        self.assertEqual(type(response.context['latest_publi']), QuerySet)
        self.assertEqual(len(response.context['latest_publi']), 3)
        self.failUnlessEqual(response.status_code, 200)
        self.assertTemplateUsed('microlly_app/index.html')

    def test_post_page(self):
        # Published news
        publis = Publication.objects.first()
        response = self.client.get(reverse('microlly_app:post', kwargs={'slug': Publication.slug}))
        self.assertContains(response, Publication.title)
        self.assertEqual(type(response.context['publi']), Publication)
        self.failUnlessEqual(response.status_code, 200)
        self.assertTemplateUsed('microlly_app/index.html')

