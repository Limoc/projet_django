from __future__ import unicode_literals
from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User
# Create your models here.

class Publication(models.Model):
    title = models.CharField(max_length=150)
    slug = models.SlugField()
    body = models.TextField(max_length=450)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now_add=True)
    def  __str__(self):
        return self.title
    def preview(self):
        return self.body[:50]+'...'
    def _get_unique_slug(self):
        slug = slugify(self.title)
        unique_slug = slug
        num = 1
        while Publication.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug
    def save(self, *args, **kwargs):
        self.slug = self._get_unique_slug()
        super().save(*args, **kwargs)



    class Meta:
        ordering = ['update_date', 'creation_date','title', ]
        verbose_name = 'Publication'
        verbose_name_plural = 'Publications'


